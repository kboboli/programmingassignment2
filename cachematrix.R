# The function calculates inverted matrix of x and stores it in s variable
# Arguments: a matrix
# Returns: list of 4 functions
# set: sets value of x (replaces x by y); sets s to NULL
# get: gets value of x
# setsolve: sets value of solve calculation
# getsolve: gets calculated value by solve function
# list: a list of 4 functions

makeCacheMatrix <- function(x = matrix()){
    
    s <- NULL
    
    set <- function(y){
        x <<- y
        s <<- NULL
    }
    
    get <- function() x
    
    setsolve <- function(solve) s <<- solve
    
    getsolve <- function() s
    
    list(set = set, get = get, setsolve = setsolve, getsolve = getsolve)
}

# The function checks if the inverse of a matrix had been already calculated
# If so, it returns cached inverse matrix
# If not, calculates inverse of the matrix

cacheSolve <- function(x, ...){
    
    s <- x$getsolve()
    
    if(!is.null(s)) {
        message("getting cached data")
        return(s)
    }
    
    else {
        
        data <- x$get()
        s <- solve(data, ...)
        x$setsolve(s)
        
        return(s)
    }
    
}
